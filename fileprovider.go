package plugins

import (
	"io"
	"net/url"
)

// FileProvider implements the interface to access files
type FileProvider interface {
	// Initialize the file provider from the given options and config
	Initialize(opts map[string]string) error

	// Save a io.Reader and return a filePath (or id)
	// Optionally allow overwriting files
	Save(fileName string, reader io.Reader, overwrite bool) (filePath string, written int64, err error)

	// Delete a file by the given path
	Delete(filePath string) (deleted bool, err error)

	// Exists checks for file existance
	Exists(filePath string) (exists bool, err error)

	// PublicURL returns the public (http) path of the file
	PublicURL(filePath string) (publicURL *url.URL, err error)
}
