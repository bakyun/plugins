package plugins

import "errors"

var (
	ErrIDAlreadyRegistered = errors.New("id already registered")
	ErrIDNotFound          = errors.New("id not found")
)
